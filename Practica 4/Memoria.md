# Practica 4: Fonética y Fonología


## Ejercicio 1: Formantes

   Abrir el archivo aeiou-apellido.wav que grabaron. Ver como difieren los espectrogramas de las vocales. Prestar especial atención a los formantes
   
   
   ![*espectrograma*](Ejercicio 1/Captura de pantalla de 2018-12-12 12-22-48.png)
   
   
   Podemos ver como cada vocal presenta una combinación de frecuencias diferente: 
   
   - /a/ presenta un formante principal que abarca las frecuencias de 0-2000 Hz aproximadamente. También se aprecian picos de menos intensidad en las frecuencias 3100-3500 Hz, y 4000-5000 Hz.
   
  - /e/ tiene un formante de 0-1200 Hz; y tres menores muy diferenciados de 2300-3000 Hz, 3300-4000 Hz y 4300-5000 Hz
  - /i/ tiene un formante de 0-1000 Hz, y tres menores entre las bandas de 3000-5000 Hz
  - /o/ tiene un único formante de 0-1500 Hz
  - /u/ tiene un único formante de 0-1200 Hz


## Ejercicio 2: Diptongos, triptongos, sordas y sonora

  Grabar los diptongos /ai/, /au/, /ia/, /ua/, /ui/, /iu/ y con los
triptongos /uai/, /uei/

- Grabar diferentes clases de consonantes (modo de articulación; sonora vs. muda) con el mismo contexto vocálico: /aba/, /aka/, /ata/, /ama/, /asa/, /afa/, etc.
- Comparar los espectrogramas.

  ![*espectrograma aga*](Ejercicio 2/aga.png)

  ![*espectrograma aja*](Ejercicio 2/aja.png)

  ![*espectrograma axa*](Ejercicio 2/axa.png)

  ![*espectrograma aya*](Ejercicio 2/aya.png)

  ![*espectrograma ara*](Ejercicio 2/ara.png)

  ![*espectrograma afa*](Ejercicio 2/afa.png)

  ![*espectrograma asa*](Ejercicio 2/asa.png)

  
  	Se aprecia claramente la diferencia entre consonantes sonoras y mudas, siendo muy destacable la consonante muda en /axa/ y /asa/, y la consonante sonora en /ara/ y /aya/. 
   
   También se percibe la dispersión del formante de la /a/ en conjunción con las consonantes que le acompañan y preceden: En /asa/, las dos /a/ se muestran prácticamente simétricas, similar en /afa/. En /ara/, prácticamente no se distingue el comienzo y fin de la /a/, y en /axa/ la /a/ final prácticamente desaparece.
   

   
## Ejercicio 3: Anotación de Audio en Praat. Formato TextGrid

  - Seleccionar un objeto Sound en la lista
   
     Hemos usado como sonido la grabación del diptongo /iu/

  - Annotate → To TextGrid... (Point vs. interval tiers)
  - Crear un TextGrid con dos capas: una de puntos y otra de intervalos (usar cualquier nombre para identificarlas).
  - Seleccionar objetos *Sound* y *TextGrid*; *View & Edit*
  - Agregar etiquetas de tipos punto e intervalo
	  + Punto: click en un circulo para agregar.
	  + Intervalo: click en un circulo para definir un extremo.
	  + Una vez seleccionada la etiqueta, tipear un texto.
  - No olvidar guardar antes de cerrar Praat.
  - Archivos de ejemplo: difonos-ai.{wav,TextGrid}
  
  ![*textgrid*](Ejercicio 3/textgrid.png)
