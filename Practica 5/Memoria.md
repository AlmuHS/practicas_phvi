# PRACTICA 5: Prosodia

## Ejercicio 1: *comoaca.wav* y *nocantesvictoria.wav*

   - Variantes de “como acá” y de “no cantes victoria”.
   - Estudiar pitch track, intensidad, duración de cada silaba.
    
      En la primera versión de "como acá":
       - el pitch track es de 124,96 Hz
       - La intensidad media es de 73,23 dB
       - la silaba /co/ dura 0.109 s.
       - la silaba /mo/ dura 0.100 s
       - /a/ dura 0.044 s
       - /ca/ dura 0.23 s
       
      En la segunda versión de "como acá":
        - El pitch track es de 97.38 Hz
        - La intensidad es de 70.39 dB
        - La silaba /co/ dura 0.108 s
        - La silaba /mo/ dura 0.093 s
        - /a/ dura 0.047 s
        - /ca/ dura 0.247 s
         
   - Intentar caracterizar la prosodia de:
        - *comoaca.wav*: pregunta vs. afirmación
        
           - En la primera pregunta, el acento está en la primera /o/. La palabra "cómo" tiene una tasa de habla mas lenta que la palabra "acá", y el tono va de grave a agudo, elevándose en el fonema /cá/.
           
           - En la segunda pregunta, el acento está en la última /a/, la tasa del habla de la palabra se mantiene uniforme durante toda la frase (aunque es algo mas rápida que la anterior). El tono se eleva durante el fonema /co/, y se mantiene constante durante el resto de la frase.
                      
           - En la primera afirmación,  el tono es mas grave, la intensidad es mas alta, y la tasa del habla es mas lenta. Se aprecia un fuerte acento en la primera /o/, pero sin aumentar el tono.
           
           - En la segunda afirmación, el tono es mas agudo, y se va elevando a lo largo de la frase. La tasa del habla es algo mas rápida que en la primera afirmación, pero mas lenta que en las preguntas. 

        - *comoaca.wav*: significado (*igual que acá* vs. *almuerzo acá*)
        
           - La primera afirmación se corresponde al significado *almuerzo acá*, mientras que la segunda se corresponde a *igual que acá*.
        
        - nocantesvictoria.wav: la pausa al final de “cantes”
       
	         - En la primera frase se indica a una persona llamada Victoria que no cante; mientras que la segunda se refiere a la frase hecha "no cantes victoria", referente a que aún puede suceder algo malo.
        


## Ejercicio 2: Manipulación de la prosodia

  - Seleccionar un objeto de tipo 'Sound'
  - Manipulate → To manipulation... (Usar rango tonal 100-400Hz.)
  - View & Edit
  - Pitch → Stylize pitch (2st)
  - Para modificar el tono, en la capa “Pitch manip":
	  - Arrastrar los puntos. Agregar puntos con:  click, CTRL+T.
  - Para modificar la duracion, en la “Duration manip”:
	  + Arrastrar los puntos. Agregar puntos con: click, CTRL+D  
	  
	  
 ![*Manipulando nocantesvictoria*](Ejercicio 2/nocantesvictoria.png)	  
 
## Ejercicio 3: Manipulación de la prosodia

  Abrir cena.wav. Modificar su prosodia para que suene a una pregunta (mejor aun si suena indignada)
  	 
  Para que suena a pregunta, incrementamos el pitch gradualmente, poniendo un acento en la /o/ de "interrumpió", e incrementando el tiempo en esa sílaba.
  	 
  ![*Manipulando cena.wav*](Ejercicio 3/cena.png)
  
## Ejercicio 4: Manipulación de la prosodia

  Abrir lamparita.wav. Modificar su prosodia para que diga “alcanza” en lugar de “alcanzá”.
  
  En la sílaba /can/, hemos incrementado el pitch e igualado los dos puntos que la forman. Además, hemos reducido el pitch en /za/.
  
  ![*Manipulando lamparita.wav*](Ejercicio 4/lamparita.png)
  
  