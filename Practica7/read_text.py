from silabeo import Silabeo

file = './text'


#Read lines from a file and save It in a list
def read_file(file):
	with open(file, 'r') as reader:
		strings = [];
		for line in reader:
			strings.append(line.rstrip());
	return strings;

#Get words of a line and save It in a list
def get_words(line):
	words_aux = line.replace(".", " .");
	words_aux = words_aux.replace(",", " ,")
	words = words_aux.split(" ") ; 

	return words;

#Get syllable of a line and save It in a list
def get_syllable(word):
	word1 = str(word);
	syllable=Silabeo.parse(word1);
	return syllable;


#strings = read_file(file)
#print(strings)

#words = get_words(strings[1]);

#print(words)

#syllable = get_syllable(words[2]);
#print(syllable);			


