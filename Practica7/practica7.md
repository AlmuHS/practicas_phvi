# Práctica Final PHVI
## Implementación de un sistema de Texto a Voz

## Autora

Almudena García Jurado-Centurión  
almudena.juradocenturion@alu.uhu.es


## Implementación
El sistema de texto a voz está programado íntegramente en Python 3.

Para la implementación del parser, se ha utilizado la librería Silabeo, desarrollada por ajlopez, y descargada de [este enlace](https://github.com/ajlopez/TddRocks/tree/master/Python/Silabeo). Esta librería permite separar una palabra en sílabas.

El fichero `read_text.py` se encarga de leer el texto línea a línea desde el fichero, separarlo en palabras, y pasárselo a Silabeo para obtener las sílabas. 

El texto se almacena en el fichero `text`, dentro del mismo directorio.

Finalmente, el fichero `main.py` se encarga de implementar el TTS, concatenando los audios de cada dífono y generando un fichero de audio con el nombre `phrase.wav`. Para saber el audio correspondiente a cada dífono, se usa una estructura diccionario, que relaciona cada dífono con su audio correspondiente. 

Para concatenar los audios se usa la librería pydub, instalable desde pip con el comando

	pip3 install pydub --user
	
## Características adicionales
	
El sistema TTS detecta los caracteres "," y ".", generando un silencio de unos pocos milisegundos al detectar uno de ellos (de un tiempo mayor en el caso del punto que en el de la coma, y en ambos casos superior al tiempo entre palabras). Además, para facilitar el parseo, se añade un "." al final de cada frase (en caso de que esta no la tenga).

También admite tildes, que han sido generadas subiendo la intensidad en las sílabas correspondientes. 


## Grabación y procesamiento de los dífonos

Los dífonos han sido grabados por una tercera persona, en un único audio con los dífonos de forma independiente. El fichero `fonos.opus` contiene los audios originales.

Dado que los dífonos tenían un pitch muy alto, han sido procesados en praat para ajustarlos a un pitch mas bajo. Los dífonos afirmativos han sido procesados de forma que presenten un pitch parabólico, ascendente al inicio y descendente al final. 

En el caso de las preguntas, se ha hecho una distinción entre "¿" y "?". 

- En "¿", se ha ajustado el pitch formando una parabólica descendente, comenzando en un pitch muy alto, y bajando progresivamente a un pitch bajo. 

- En "?" el pitch forma una parabólica ascendente, empezando en un pitch bajo y subiendo a un pitch muy alto. 

También se ha modificado la duración, pasando de una duración corta a larga en "¿" y de larga a corta en "?".


## Ejecución

Para ejecutar el programa, hay que instalar la librería pydub y ejecutar el script `main.py` desde Python, con el comando 
		
	python main.py

En caso de ejecutarse desde GNU/Linux, es recomendable usar el comando
	
	python3 main.py
	

El programa mostrará por pantalla la lista con las palabras a reproducir y, tras generar el audio, lo reproducirá y lo guardará en `phrase.wav`


## Pruebas

La práctica ha sido desarrollada y probada en Debian GNU/Linux 10 (Buster), con la versión de Python 3.7.2. 