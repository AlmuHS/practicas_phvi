import read_text as read_text
from pydub import AudioSegment, playback


record = {'ka':'ka.wav','la':'la.wav', 'ma':'ma.wav','pa':'pa.wav','sa':'sa.wav',}
record.update({'¿sa':'¿sa.wav', 'sa?':'sa?.wav', '¿ka':'¿ka.wav' ,'ka?':'ka?.wav', \
'¿la':'¿la.wav', 'la?':'la?.wav', '¿pa':'¿pa.wav', 'pa?':'pa?.wav', '¿ma':'¿ma.wav', 'ma?':'ma?.wav'})

filename = './text'

def gen_wordlist_of_file(filename):
	#Get lines of file and save It in a list
	strings = read_text.read_file(filename);

	#for each line, save words
	words = [];
	for line in strings:
		words = words + read_text.get_words(line)

		#if phrase hasn't stop at end, add this
		if "." not in words[len(words)-1]:
			words = words + ["."]

	return words;
#end function

def get_record_of_a_wordlist(wordlist):
	sound = AudioSegment.empty()

	for word in wordlist:
		if word != "." and word != ",":
			word = word.lower()
			syllable = read_text.get_syllable(word)

			for s in syllable:
				if s.find("á") != -1:
					s = s.replace("á", "a")
					audio = AudioSegment.from_wav(record[s]);
					sound = sound + (audio+5.8)
				else:
					audio = AudioSegment.from_wav(record[s]);
					sound = sound + audio
			
			#Little silent between words
			sound = sound + AudioSegment.silent(duration=50)
		
		elif word == ",":
			sound = sound + AudioSegment.silent(duration=300)
		elif word == ".":
			sound = sound + AudioSegment.silent(duration=1000)

	sound.export('phrase.wav', format='wav')
	playback.play(sound)
#end function


words = gen_wordlist_of_file(filename)
print(words)
get_record_of_a_wordlist(words)


