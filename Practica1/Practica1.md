# Practica 1: SoX para manipular archivos de audio

En esta práctica usaremos la herramienta SoX para manipular ficheros de audio, y comparar sus cambios

## Ejercicios 

**1. Escuchar bach.wav (44.1kHz, 16bits)**
   - Fragmento de *Partita en Sol Mayor* de J. S. Bach
   
          play bach.wav 

**2. Bajar sampling rate a 16, 8, 4 kHz y comparar.**

   - Bajar a 16 KHz
   
         sox bach.wav -r 16000 bach_16.WAV
         
   - Bajar a 8 KHz
   
         sox bach.wav -r 8000 bach_8.WAV
         
   - Bajar a 4 KHz 
   
         sox bach.wav -r 4000 bach_4.WAV
         
   - Comparar
     
     Al bajar la frecuencia, se reduce la percepción de los picos de audio, especialmente en los agudos, que se agravan dando un sonido "enlatado".

     Esto también produce una reducción del efecto polifónico, donde se pueden distinguir varios hilos de sonido en el audio original, para pasar a un solo hilo musical en el de 4 KHz.


**3. Subir sampling rate de 4 kHz a 44.1kHz.**

       sox bach_4.wav -r 44100 bach_44-1.WAV

   - **¿Por qué no vuelve a estar en buena calidad?**
   
     Porque, al bajarle la calidad, hemos reducido la tasa de muestreo, que ha reducido la    resolución del audio. Aunque subamos la frecuencia, al haber reducido previamente el muestreo, esa información no se puede volver a recuperar.

**4. Aplicar filtro high- pass de 8 Khz a:**

   - **Audio original a 44.1 kHz.**

           sox bach.wav bach_HP.WAV sinc 8000

   - **Audio resampleado a 16 kHz**

     No se puede aplicar el filtro *high-pass*, porque supera los límites de frecuencia del audio (la frecuencia filtrada debe ser menor al *sample-rate*)

     Reducimos el filtro a 4 KHz:

           sox bach_16.WAV bach_HP16.WAV sinc 4000

 **5. Crear espectrogramas de los audios y comparar**

   - Audio original a 44.1 KHz

         sox bach.wav -n spectrogram -o bash_original.png

      ![*Audio original*](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bach_original.png)

   
   - Audio a 16 KHz

         sox bach_16.WAV -n spectrogram -o bach16.png

     ![*Audio a 16 KHz*](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bach16.png)

   - Audio a 8 KHz

         sox bach_8.WAV -n spectrogram -o bach8.png

     ![*Audio a 8KHz*](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bach8.png)


   - Audio a 4 KHz

         sox bach_4.WAV -n spectrogram -o bach4.png

      ![*Audio a 4KHz*](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bach4.png)

   - Audio recompuesto a 44.1 KHz desde 4 KHz

         sox bach_44-1.WAV -n spectrogram -o bach44-1.png

      ![*Audio recompuesto a 44.1 KHz desde 4 KHz*](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bach44-1.png)

   - Audio de 44.1 KHz con High-Pass de 8 KHz

         sox bach_HP.WAV -n spectrogram -o bachHP.png

      ![Audio 44.1 KHz con High-pass](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bachHP.png)

  - Audio de 16 KHz con High-Pass de 4 KHz

         sox bach_HP16.WAV -n spectrogram -o bachHP16.png

      ![Audio 16 KHz HighPass](https://gitlab.com/AlmuHS/practicas_phvi/raw/master/Practica1/imagenes/bachHP16.png)