# PRACTICA 6: Sintesis de Formantes con Klatt



## Ejercicio 1: Síntesis de formantes con Klatt y ayuda de Praat


  Generar  los  siguientes  sonidos  usando  la  interfaz  web  del  sintetizador  de  formantes Klatt (http://www.asel.udel.edu/speech/tutorials/synthesis/Klatt.html)

  - Las cinco vocales: /a/ /e/ /i/ /o/ /u/.
 
    Con Praat, medir en aeiou.wav los formantes de cada vocal.  
    View & Edit; Seleccionar vocal;Formant > Get first/second/third formant.
       
       - Vocal /a/: 
	       + Primer formante: 736.227963915481 Hz
	       + Segundo formante: 1355.697697065095 Hz
	       + Tercer formante: 2652.2281249324988 Hz
	       
			![*Formantes /a/*](Ejercicio 1/a.png)

       - Vocal /e/:
	       + Primer formante: 427.1123403905589 Hz
	       + Segundo formante: 2026.5398875428916 Hz
	       + Tercer formante: 2685.6999403030477 Hz
	       
	       ![*Formantes /e/*](Ejercicio 1/e.png)
	       
       - Vocal /i/: 
	       + Primer formante: 416.3344771832673 Hz
	       + Segundo formante: 2375.6697081755424 Hz
	       + Tercer formante: 3048.755101976137 Hz
	       
			![*Formantes /i/*](Ejercicio 1/i.png)

       - Vocal /o/:
	       + Primer formante: 563.1427716830914 Hz
	       + Segundo formante: 1052.649747427836 Hz
	       + Tercer formante: 2951.475644804801 Hz
	       
	       ![*Formantes /o/*](Ejercicio 1/o.png)
	       
       - Vocal /u/:
	       + Primer formante: 444.9128528117391 Hz
	       + Segundo formante: 1725.9822215801341 Hz
	       + Tercer formante: 3071.6292665806086 Hz
	       
	       ![*Formantes /u/*](Ejercicio 1/u.png)
	     
	   Una vez calculados los formantes, pasamos a generar los sonidos mediante Klatt. Para ello, debemos escribir los scripts correspondientes a cada vocal, usando los formantes que acabamos de calcular.
	     
	   - Script /a/: 
	   
	     		TIME=0; AV=80; F1=736; F2=1355; F3=2652
				TIME+500
				END 
				
		   Sonido generado: [/a/](Ejercicio 1/a.wav)
	     
	   - Script /e/:
	   			
	   			TIME=0; AV=80; F1=427; F2=2026; F3=2685
				TIME+500
				END
				 
			Sonido generado: [/e/](Ejercicio 1/e.wav)
			
	   - Script /i/:
	   
	   			TIME=0; AV=80; F1=416; F2=2375; F3=3048
				TIME+500
				END
				
			Sonido generado: [/i/](Ejercicio 1/i.wav)
				
	   - Script /o/:
	   			
	   			TIME=0; AV=80; F1=563; F2=1052; F3=2951
				TIME+500
				END
				
		    Sonido generado: [/o/](Ejercicio 1/o.wav)
		     
	   - Script /u/:
	       
	          TIME=0; AV=80; F1=444; F2=1725; F3=3071
			  TIME+500
			  END
		
		   Sonido generado: [/u/](Ejercicio 1/u.wav)
	      
  - Diptongos (/iu/ /ui/ /ai/ /ia/, etc.).
  
  	Nos basaremos en los diptongos grabados en las prácticas anteriores, combinando los formantes de las vocales que lo forman.
  
    - Diptongo /iu/:
       
        - /i/:
       
       		- Primer formante: 324.4333878552927 Hz
       		- Segundo formante: 1579.2189464972673 Hz
       		- Tercer formante: 3200.0423984085196 Hz
       		- Tiempo: 0.179135 s
       		
   		- /u/:
	   		- Primer formante: 540.9987432818473 Hz
	   		- Segundo formante: 2072.135334325366 Hz
	   		- Tercer formante: 3291.5778908465722 Hz
	   		- Tiempo: 0.159488 s
	   	
	   	 - Script /iu/:
		   	 
		   	      TIME=0; AV=80; F1=324; F2=1579; F3=3200
				  TIME+500
				  TIME=500; AV=80; F1=540; F2=2072; F3=3291
				  TIME+250
				  END
				  
			Sonido resultante: [/iu/](Ejercicio 1/iu.wav)
   
   	- Diptongo /ui/:
   	
	   	+ /u/:
		   	* Primer formante: 361.9078708803551 Hz
		   	* Segundo formante: 893.1549964383875 Hz
		   	* Tercer formante: 2608.5473848834376 Hz
		   	* Tiempo: 0.205259 s
		   	
	   	+ /i/: 
		   	* Primer formante: 462.3896439632145 Hz
		   	* Segundo formante: 2122.571783417334 Hz
		   	* Tercer formante: 3136.6199413521867 Hz
		   	* Tiempo: 0.203410 s
		   	
	   	+ Script /ui/:
	   	
	   	 		  TIME=0; AV=80; F1=362; F2=893; F3=2608
				  TIME+250
				  TIME=250; AV=80; F1=462; F2=2122; F3=3136
				  TIME+250
				  END
	   	
	   		Sonido resultante: [/ui/](Ejercicio 1/ui.wav)
	   		
    -	Diptongo /ai/: 
    
	    +	/a/:
		    *	Primer formante: 859.258961699921 Hz
		    *	Segundo formante: 1726.641252146491 Hz 
		    *	Tercer formante: 3262.601692750394 Hz
		    *	Tiempo: 0.162550
	    +	/i/:
		 	*	Primer formante: 651.3199721389799 Hz
		    *	Segundo formante: 1895.1464998101733 Hz
		    *	Tercer formante: 3137.8952563237153 Hz
		    *	Tiempo: 0.228301
		    
	    +	Script /ai/
	    
		     	 TIME=0; AV=80; F1=859; F2=1726; F3=3262
				 TIME+250
				 TIME=250; AV=80; F1=651; F2=1895; F3=3137
				 TIME+500
				 END
  
  			Sonido resultante: [/ai/](Ejercicio 2/ai.wav).
  			
  	- Diptongo /ia/:
  	
  		- /i/:
  	   		- Primer formante: 373.6582247357948 Hz
  	   		- Segundo formante: 1503.7632271520417 Hz
  	   		- Tercer formante: 3241.3944778786235 Hz
  	   		- Tiempo: 0.227690 s
  	   - /a/: 
  	   		- Primer formante: 793.0457870946218 Hz
  	   		- Segundo formante: 1771.10356165569 Hz
  	   		- Tercer formante: 3086.220553243078 Hz 
  	   		- Tiempo: 0.139894 s
  	   
  	   - Script /ia/:
  	   
  	   		TIME=0; AV=80; F1=373; F2=1503; F3=3241
			TIME+500
			TIME=500; AV=80; F1=793; F2=1771; F3=3086
			TIME+500
			END
				 
          Sonido resultante: [/ia/](Ejercicio 2/ia.wav).
        
    - Diptongo /ua/:
      
      - /u/:
      
      	- Primer formante: 400.66141924543274 Hz 
      	- Segundo formante: 832.6114679039287 Hz
      	- Tercer formante: 2828.6077918477836 Hz
      	
      - /a/:
      
      	- Primer formante: 832.7605479756409 Hz
      	- Segundo formante: 1241.9615910641637 Hz
      	- Tercer formante: 2777.728896608951 Hz
      
      - Script /ua/: 
      
			TIME=0; AV=80; F1=400; F2=832; F3=2828
			TIME+500
		    TIME=500; AV=80; F1=832; F2=1241; F3=2777
			TIME+500
			END
  	   
  	    Sonido resultante: [/ua/](Ejercicio 2/ua.wav).
  	    
	- Diptongo /au/:
	
		- /a/: 
		
		   - Primer formante: 796.1845305578134 Hz
		   - Segundo formante: 1558.8043496092291 Hz
		   - Tercer formante: 3099.5684536983695 Hz
		   
		 - /u/:
		 
		   - Primer formante: 646.0924986654277 Hz
		   - Segundo formante: 1637.6110958912404 Hz
		   - Tercer formante: 3091.79267802485 Hz
		   
		 - Script /au/:
		 
		 		TIME=0; AV=80; F1=796; F2=1558; F3=3099
				TIME+500
		   		TIME=500; AV=80; F1=646; F2=1637; F3=3091
				TIME+500
				END
	
  - La vocal /a/, ahora con tres contornos de entonación distintos (ej: ascendente, sostenido, descendente). Para esto, modificar F0 (frecuencia fundamental).
  
   	- /a/ ascendente:
   	
   		  TIME=0; AV=80; F0=107; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=200; AV=80; F0=157; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=400; AV=80; F0=200; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=600; AV=80; F0=250; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=800; AV=80; F0=300; F1=736; F2=1355; F3=2652
		  TIME+200
		  END  
		  
	  Sonido resultante: [/a/](Ejercicio 1/a_inc.wav)
	  
    - /a/ sostenida: 
  
          TIME=0; AV=80; F0=107; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=200; AV=80; F0=160.5; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=400; AV=80; F0=240.75; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=600; AV=80; F0=361.125; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=800; AV=80; F0=541.6875; F1=736; F2=1355; F3=2652
		  TIME+200
		  END  
    
      Sonido resultante:  [/a/](Ejercicio 1/a_sost.wav)
  
  
    - /a/ descendente:
  
          TIME=0; AV=80; F0=100; F1=736; F2=1355; F3=2652
		  TIME+200; 
		  TIME=400; AV=80; F0=80; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=600; AV=80; F0=60; F1=736; F2=1355; F3=2652
		  TIME+200
		  TIME=800; AV=80; F0=40; F1=736; F2=1355; F3=2652
		  TIME+200
		  END  
		
		Sonido resultante: [/a/](Ejercicio 1/a_dec.wav)
	
	
 
 