# Práctica 3: Variables Acústicas en Praat: Intensidad y Pitch. Scripts

## Ejercicio 1: Intensidad y nivel tonal en Praat
 - Abrir `lamparita.wav`
 - Hacer clic en "View & Edit"

   ![View&Edit](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-33-55.png)

 - Menú *Intensity*
     1. Activar *Show intensity*. En *Intensity Settings* poner 50-100dB
         ![Intensity Settings](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-34-44.png)

     2. Seleccionar un segmento de habla
         
         ![Seleccion segmento](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-37-45.png)


     3.  Click en *Intensity Listing* y en *Get intensity*

         ![Intensity](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-38-31.png)

         *Intensity listing* muestra un valor de 74.86 Hz, mientras que *Get Intensity* muestra un valor de 77.15 Hz
        
         ![Intensity listing](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-38-41.png)

         ![Get Intensity](Practica3/Actividad 1/Intensity/Captura de pantalla de 2018-12-03 19-40-30.png)

 - Menú *Pitch*
     1. Activar *Show pitch*. En *Pitch Settings* poner 75-500 Hz
       ![Show Pitch](Practica3/Actividad 1/Pitch/Captura de pantalla de 2018-12-03 19-44-56.png)
       ![Pitch Settings](Practica3/Actividad 1/Pitch/Captura de pantalla de 2018-12-03 19-45-12.png)

     2. Seleccionar un segmento de habla  

         Mantenemos el mismo segmento de habla que en el apartado anterior
     3. Click en *Pitch listing* y en *Get pitch*.

        ![Get Pitch](Practica3/Actividad 1/Pitch/Captura de pantalla de 2018-12-03 19-47-25.png)

        ![Get Pitch 2](Practica3/Actividad 1/Pitch/Captura de pantalla de 2018-12-03 19-47-30.png)

        ![Pitch listing](Practica3/Actividad 1/Pitch/Captura de pantalla de 2018-12-03 19-51-37.png)

         Ambos dan resultados muy similares

         - Para la primera y segunda /u/ (en “subi” y “un”), estimar a mano su F0 usando sólamente su forma de onda. Comparar con los cómputos de Praat.

          Comenzamos extrayendo los patrones de ambas /u/. El patrón de la primera /u/ es el siguiente:
          ![Primera U](Practica3/Actividad 1/Comparar_U/primera_u_patron.png)

          En la primera /u/, el periodo del patrón complejo mas pequeño es de 0,00305 segundos. Esto nos da una frecuencia F0 de 327,868852459 Hz. Praat en cambio nos indica 320.1768606241254 Hz (maximum pitch in SELECTION).

          El de la segunda /u/ es:
          ![Segunda U](Practica3/Actividad 1/Comparar_U/segunda_u_patron.png)

          En la segunda /u/, el periodo del patrón complejo mas pequeño es de 0,006275, lo cual nos da una frecuencia F0 de 159,362549801 Hz. Praat en cambio, nos indica 167.42728226170283 Hz (mean pitch in SELECTION)        
        
## Ejercicio 2: Pitch track vs. Espectrograma
  - Abrir `a.wav`

     ![a.wav](Practica3/Actividad 2/Captura de pantalla de 2018-12-04 19-27-28.png)

  - Escuchar (es un sonido /a/ largo, subiendo y bajando el tono, primero de grave a agudo y luego al revés)
  - Editarlo.

     ![Editar a.wav](Practica3/Actividad 2/Captura de pantalla de 2018-12-04 19-28-02.png)

  - Visualizar sólo el espectrograma.

     ![Espectrograma](Practica3/Actividad 2/Captura de pantalla de 2018-12-04 19-28-29.png)

     - ¿Cuál es el eje de referencia del espectrograma?

         El eje de referencia es 0-5000 Hz
     - En *Spectrogram settings* poner rango = 0-5000 Hz

         ![Rango espectograma](Practica3/Actividad 2/Captura de pantalla de 2018-12-04 19-32-52.png)

     - ¿Qué ocurre con el espectrograma cuando el tono sube/baja?

         En ambos casos, se aprecian tres franjas negras. Pero, al subir el tono, las tres franjas quedan muy diferenciadas, y la franja de mayor frecuencia se va mostrando mas oscura.
         Por el contrario, al bajar el tono estas franjas se muestran mas difuminadas, y la franja superior casi desaparece

     - Visualizar solo el Pitch track
 
         ![Pitch track](Practica3/Actividad 2/Captura de pantalla de 2018-12-05 11-38-25.png)

         - ¿cual es el eje de referencia del pitch track?

             El eje de referencia es de 150-500 Hz
         - En *pitch settings* poner un rango tonal de 150-500 
         
             ![Pitch settings](Practica3/Actividad 2/Captura de pantalla de 2018-12-05 11-50-32.png)

         - ¿que ocurre con el Pitch track?

             El pitch track se muestra discontinuo
         - Repetir con 50-150 Hz.

             Se produce un pitch doubling en la primera parte, y un pitch halving en la segunda

             ![Pitch track ajustado](Practica3/Actividad 2/Captura de pantalla de 2018-12-05 11-45-25.png)

## Ejercicio 3: Scripting

 - Desde la historia de la sesión:
     - Praat → new Praatscript → Edit → Paste history

     ![Paste history](Practica3/Actividad 3/Captura de pantalla de 2018-12-05 12-01-27.png)

     ![Nuevo script](Practica3/Actividad 3/Captura de pantalla de 2018-12-05 12-01-49.png)

  - Modificar scripts existentes.
  - En una terminal, ejecutar los siguientes comandos:
     - `praat duration.praat lamparita.wav`
         - Devuelve la duración del archivo made1.wav, en segundos.

         ![comando 1](Practica3/Actividad 3/Captura de pantalla de 2018-12-05 12-08-27.png)

     - `less duration.praat`
         - Muestra el archivo duration.praat.


            ```
            $ less duration.praat

            # Praat script que toma como input un archivo de audio (.wav)
            # y devuelve su longitud en segundos.

            # Argumento: archivo de audio.
            form Input parameters for sound length
            word file .wav
            endform

            # Los objetos 'long sound' no se levantan a memoria.
            Open long sound file... 'file$'

            # Calcula la duracion.
            dur = Get duration

            # La imprime y termina.
            echo 'dur:4'
            ```

  - En una terminal ejecutar:
       - `praat acoustics.praat lamparita.wav 0.5 1.0 75 500`
          - Computa un conjunto de mediciones acústicas para lamparita.wav entre 0.5 y 1.0 segundos, usando rango tonal 75-500Hz.
         
         ![comando 3](Practica3/Actividad 3/Captura de pantalla de 2018-12-05 12-14-06.png)

## Ejercicio 4: Intensidad y nivel tonal en Praat.

Abrir el archivo `hola.wav` en Praat y tomar nota del comienzo y final de la /o/ en cada instancia de la 
palabra “hola”. ¡Sean precisos!

 La primera /o/ va desde 0.0 hasta 0.308655 s
 La segunda /o/ va desde 0.971313 hasta 1.085860 s
 La tercera va desde 1.879383 hasta 2.037821 s

- Ejecutar acoustics.praat sobre cada /o/, donde (minpch,maxpch) = (50,300) para hombres, o (75,500) para
mujeres

    ```
    $ praat acoustics.praat hola.wav 0.0 0.308655 50 300
    SECONDS:0.309
    F0_MAX:251.802
    F0_MIN:71.750
    F0_MEAN:117.857
    F0_MEDIAN:78.319
    F0_STDV:72.412
    ENG_MAX:67.227
    ENG_MIN:44.766
    ENG_MEAN:58.976
    ENG_STDV:8.547
    VCD2TOT_FRAMES:0.824

    $ praat acoustics.praat hola.wav 0.971313 1.085860 50 300
    SECONDS:0.115
    F0_MAX:--undefined--
    F0_MIN:--undefined--
    F0_MEAN:--undefined--
    F0_MEDIAN:--undefined--
    F0_STDV:--undefined--
    ENG_MAX:--undefined--
    ENG_MIN:--undefined--
    ENG_MEAN:--undefined--
    ENG_STDV:--undefined--
    VCD2TOT_FRAMES:--undefined--

    $ praat acoustics.praat hola.wav 0.971313 1.085860 75 500
    SECONDS:0.115
    F0_MAX:100.531
    F0_MIN:91.394
    F0_MEAN:95.237
    F0_MEDIAN:93.859
    F0_STDV:3.874
    ENG_MAX:78.240
    ENG_MIN:76.819
    ENG_MEAN:77.531
    ENG_STDV:0.710
    VCD2TOT_FRAMES:1.000

    $ praat acoustics.praat hola.wav 1.879383 2.037821 50 300
    SECONDS:0.158
    F0_MAX:135.688
    F0_MIN:111.612
    F0_MEAN:124.957
    F0_MEDIAN:127.303
    F0_STDV:9.521
    ENG_MAX:90.179
    ENG_MIN:89.562
    ENG_MEAN:89.871
    ENG_STDV:0.436
    VCD2TOT_FRAMES:1.000
    ```

- Comparar la intensidad en cada caso


## Ejercicio 5: Intensidad y nivel tonal en Praat

Crear una onda periódica compleja formada por dos ondas simples de
400 y 500 Hz.

 - New > Sound > Create sound from formula...
 - Formula: sin(2*pi*400*x) + sin(2*pi*500*x)
 
     ![Sound from formula](Practica3/Actividad 5/Captura de pantalla de 2018-12-06 18-37-34.png)

     - Visualizar la forma de onda y computar F0 a mano.

       ![Forma de onda](Practica3/Actividad 5/Captura de pantalla de 2018-11-19 19-40-32.png)

       ![Forma de onda zoom](Practica3/Actividad 5/Captura de pantalla de 2018-11-19 19-48-24.png)

       La frecuencia F0 es de 99,373944152 Hz (1/0.010171)

     - Visualizar el pitch track: Pitch > Show pitch, y comparar con el resultado del punto anterior

       ![Pitch track](Practica3/Actividad 5/Captura de pantalla de 2018-11-19 20-03-17.png)

       El Pitch track es de 100 Hz, mientras que el resultado anterior es 99,373944152 Hz

## Ejercicio 6: Intensidad y nivel tonal en Praat

¿Dónde producimos el tono? Grabarse diciendo las notas musicales *do*, *re*, *mi*, *fa*, *sol*, *la*, *si*, *do*, en el tono correspondiente (aprox), pero **susurrando**. Editar el archivo y analizar el pitch track.

 ![Pitch track doremi](Practica3/Actividad 6/Captura de pantalla de 2018-12-07 20-18-32.png)

 - Grabar las 5 vocales, como en el archivo “aeiou.wav"

  ![aeiou](Practica3/Actividad 6/Captura de pantalla de 2018-12-07 20-48-00.png)

